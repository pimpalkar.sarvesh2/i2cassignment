#include "header.h"
#include "i2cDriver.h"

#ifndef I2C_DS3231
#define I2C_DS3231

#define BUFFER_SIZE 19
//#define I2C_ADDRESS 0x68
#define ALARM1_ONCE_PER_SECOND     0b00001111
#define ALARM1_WHEN_S_MATCH        0b00001110
#define ALARM1_WHEN_MS_MATCH       0b00001100
#define ALARM1_WHEN_HMS_MATCH      0b00001000
#define ALARM1_WHEN_DATE_HMS_MATCH 0b00000000
#define ALARM1_WHEN_DAY_HMS_MATCH  0b00010000

#define ALARM2_ONCE_PER_MINUTE     0b00000111
#define ALARM2_WHEN_M_MATCH        0b00000110
#define ALARM2_WHEN_HM_MATCH       0b00000000
#define ALARM2_WHEN_DAY_HM_MATCH   0b00001000

#define ENABLE_ALARM1 	  0
#define ENABLE_ALARM2 	  1
#define ENABLE_BOTH_ALARM 2
#define DISABLE_BOTH_ALARM 3

#define SQW_KHZ_1    0
#define SQW_KHZ_1024 1
#define SQW_KHZ_4096 2
#define SQW_KHZ_8192 3
#define SQW_STOP	 4




namespace rpi{

class RTCDevice:protected I2CDeviceDriver{
private:
	unsigned int I2CBus, I2CAddress;
	unsigned char *registers;
	//virtual int updateRegisters();

public:
	RTCDevice(unsigned int I2CBus, unsigned int I2CAddress=0x68);
	virtual void readTimeAndDate();
	virtual void setTime(bool, bool, char, char, char);
	virtual void setDate(char, char, char, char);
	virtual void setAlarm(bool, bool, char, char, char, char, char);
	virtual void readTemperature();
	virtual void dumpAll(unsigned int);
	virtual void readSetAlarms();
	virtual void setRtcInterrupt(unsigned int);
	virtual void squareWaveGenFunc(unsigned int);
	virtual void getStatusRegister();
	virtual void writeAgingOffset(signed char);
	virtual ~RTCDevice();
};

#endif //I2C_DS3231

}//namespace rpi
