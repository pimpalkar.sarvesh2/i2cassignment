#include "header.h"
#include "DS3231.h"

using namespace std;

#define HEX(x) setw(2) << setfill('0')<< hex << (int)(x)
#define BIT(n) (1<<(n))

int bcdToDec(char b) {return (b/16)*10 + (b%16);}
int decToBcd(char b) {return ((b / 10 * 16) + (b % 10));}

namespace rpi{

#define RTC_ADDR_SECS   0x00
#define RTC_ADDR_MINS   0x01
#define RTC_ADDR_HOURS  0x02 
#define RTC_ADDR_DAY    0x03
#define RTC_ADDR_DATE   0x04
#define RTC_ADDR_MONTH  0x05
#define RTC_ADDR_YEAR   0x06

#define ALARM1_SECS     0x07
#define ALARM1_MINS     0x08
#define ALARM1_HOURS    0x09
#define ALARM1_DAY_DATE 0x0A

#define ALARM2_MINS     0x0B
#define ALARM2_HOURS    0x0C
#define ALARM2_DAY_DATE 0x0D

#define CONTROL_REG 0x0E
#define STATUS_REG  0x0F
#define AGING_OFFSET_REG 0x10

/**
 * Constructor for the RTCCDevice class of the I2CDeviceDriver. It requires the bus number and I2C ID.
 * @param I2Cbus: The bus number.
 * @param I2CAddress: The device ID on the bus.
 */
RTCDevice::RTCDevice(unsigned int I2CBus, unsigned int I2CAddress):I2CDeviceDriver(I2CBus, I2CAddress){
	this->I2CAddress = I2CAddress;
	this->I2CBus = I2CBus;

}//RTCDevice

/**
 * Prints the current time and date
 */
void RTCDevice::readTimeAndDate(){
	this->registers = this->readRegisters(BUFFER_SIZE, 0x00);
	if((registers[RTC_ADDR_SECS] & BIT(6)) == 0){//am
		cout<<"Hours:"<<bcdToDec(registers[RTC_ADDR_HOURS]&0b00011111)<<"am"<<endl;
	}//if
	else{//pm
		cout<<"Hours:"<<bcdToDec(registers[RTC_ADDR_HOURS]&0b00011111)<<"pm"<<endl;
	}//esle
	cout<<"Mins:"<<bcdToDec(registers[RTC_ADDR_MINS])<<endl;
	cout<<"Secs:"<<bcdToDec(registers[RTC_ADDR_SECS])<<endl;
	string DayOfWeek[8]{ " ", "Sunday","Monday", "Tuesday","Wednesday","Thursday","Friday","Saturday"};
	cout<<"DayOfWeek:"<<DayOfWeek[bcdToDec((registers[RTC_ADDR_DAY]))]<<endl;
	cout<<"DateOfMonth:"<<bcdToDec((registers[RTC_ADDR_DAY]))<<endl;
	cout<<"Month:"<<bcdToDec((registers[RTC_ADDR_DAY]))<<endl;
	cout<<"Year:"<<"20"<<bcdToDec((registers[RTC_ADDR_YEAR]))<<endl;
}//readTime()

/**
 * Prints the current temperature
 */
void RTCDevice::readTemperature(){
	this->registers = this->readRegisters(BUFFER_SIZE, 0x00);

	int i, j;
	char sign;

	i = registers[0x11];
	j = registers[0x12];
	
	if(i & (1<<8)) //Check for sign bit
		sign = '-';
	else
		sign = '+';
		
	i = i<<2;
	j = j & 0b11000000; //Isolate the MSB 2 bits of 12h-reg

	i = i | (j>>6);

	cout<<"Temperature is:"<<sign<<((float) (i*0.25))<<"degree celcius"<<endl;
	
}//readTemperature()

/**
 * Sets the current time into the RTC device
 * @param twelveHoursFormat: 1 for 12hours, 0 for 24 hours
 * @param am: 1 for am, 0 for pm
 * @param hours, mins, secs: User to give current time
 */
void RTCDevice::setTime(bool twelveHoursFormat, bool am, char hours, char mins, char secs){
	
	if((0<=hours<=24) && (0<=mins<=60) && (0<=secs<=60)){
		this->writeReg(RTC_ADDR_SECS, secs);
		this->writeReg(RTC_ADDR_MINS, mins);
		
		if(twelveHoursFormat){
			this->writeReg(RTC_ADDR_HOURS, ((hours | BIT(6)) | BIT(5)));
		}//if
		else{//if not twelveHoursFormat format
			this->writeReg(RTC_ADDR_HOURS, hours);
		}//else
	}//if
	else{
		cout<<"SetTime: Invalid time"<<endl;
		return;
	}//else
}//setTime()

/**
 * Set Date into the RTC Device
 * @param dateOfMonth: Range: 1 to 31
 * @param month: Range: 1 to 12; i.e 1 = January, 2 = Feb etc.
 * @param year: Range: 0 to 99; units and tens place of current year
 * @param dayOfWeek: Range: 1 to 7; i.e. 1 = Sunday, 2 = Monday, etc.
*/
void RTCDevice::setDate(char dateOfMonth, char month, char year, char dayOfWeek){
	if((1<=dateOfMonth<=31) && (1<=month<=12) && (0<=year<=99) && (1<=dayOfWeek<=7)){
		this->writeReg(RTC_ADDR_DATE, decToBcd(dateOfMonth));
		this->writeReg(RTC_ADDR_MONTH, decToBcd(month));
		this->writeReg(RTC_ADDR_YEAR, decToBcd(year));
		this->writeReg(RTC_ADDR_DAY, decToBcd(dayOfWeek));
	}//if
	else{
		cout<<"SetDate: Invalid input"<<endl;
		return;
	}//else
}//setDate()

/** Set alarm into the RTC Device
 * @param twelveHoursFormat: 1: 12hours format, 0:24hours format
 * @param alarmNo(bool): 0 for Alarm1, 1 for Alarm2
 * @param hours(char): Range 
 * @param dayOfWeekORdateOfMonth: 1 to 7; i.e. 1 = Sunday, 2 = Monday, etc.
*/
void RTCDevice::setAlarm(bool twelveHoursFormat, bool alarmNo, char hours, char mins, char secs, char dayOfWeekORdateOfMonth, char alarmType){
	if((0<=hours<=24) && (0<=mins<=60) && (0<=secs<=60))
	{
		if(alarmNo)
		{//if(1) set Alarm2
			cout<<"Setting alarm 2.."<<endl;
			this->writeReg(ALARM2_MINS, (((alarmType & BIT(0)) <<7) | mins));
			
			if(twelveHoursFormat){
				this->writeReg(ALARM2_HOURS, ((((alarmType & BIT(1)) <<6) | hours)| BIT(6)));
			}//if
			else{//else 24hours format
				this->writeReg(ALARM2_HOURS, (((alarmType & BIT(1)) <<6) | hours));
			}//else
		this->writeReg(ALARM2_DAY_DATE, ((((alarmType & BIT(4)) <<4) | dayOfWeekORdateOfMonth) | BIT(6)));
		}//if(alarmNo)
		else{//if(0) set Alarm1
			cout<<"Setting alarm 1.."<<endl;
			this->writeReg(ALARM1_SECS,    (((alarmType & BIT(0)) <<7) | secs));
			this->writeReg(ALARM1_MINS,    (((alarmType & BIT(1)) <<6) | mins));
			
			if(twelveHoursFormat){
				this->writeReg(ALARM1_HOURS,   ((((alarmType & BIT(2)) <<5) | hours) | BIT(6)));
			}//if
			else{//else 24hours format
				this->writeReg(ALARM1_HOURS,   (((alarmType & BIT(2)) <<5) | hours));
			}//else
		this->writeReg(ALARM1_DAY_DATE,((((alarmType & BIT(3)) <<4) | ((alarmType & BIT(4)) <<2))  | dayOfWeekORdateOfMonth));
		}//else
	}//if
	else{
		cout<<"SetAlarm: Invalid input."<<endl;
	}//else
}//setAlarm

/* Read the set alarms */
void RTCDevice::readSetAlarms(){
		this->registers = this->readRegisters(BUFFER_SIZE, 0x00);
		
		if((registers[ALARM1_HOURS] & BIT(6)) == 0){//if 24hours format
			cout<<"Alarm1 set hours:"<<(registers[ALARM1_HOURS] & 0b00111111)<<endl;
		}//if
		else{//12Hours format
			cout<<"Alarm1 set hours:"<<(registers[ALARM1_HOURS] & 0b00011111);
			if((registers[ALARM1_HOURS] & BIT(5)) == 0){//am
				cout<<"am."<<endl;
			}//if
			else{//pm
				cout<<"pm."<<endl;
			}//else
		}//else
			
		cout<<"Alarm1 set minutes:"<<(registers[ALARM1_MINS] & 0b01111111)<<endl;
		cout<<"Alarm1 set seconds:"<<(registers[ALARM1_SECS] & 0b01111111)<<endl;
		
		if((registers[ALARM1_DAY_DATE] & BIT(6)) == 0){//Day/Date decision; Day = 1
			cout<<"Alarm1 set day:"<<(registers[ALARM1_DAY_DATE] & 0b00111111)<<endl;
		}//if
		else{//Day/Date decision; Date = 0
			cout<<"Alarm1 set date:"<<(registers[ALARM1_DAY_DATE] & 0b00111111)<<endl;
		}//else
		
		if((registers[ALARM2_HOURS] & BIT(6)) == 0){//24hours
			cout<<"Alarm2 set hours:"<<(registers[ALARM2_HOURS] & 0b00111111)<<endl;
		}//if
		else{//12hours
			cout<<"Alarm2 set hours:"<<(registers[ALARM2_HOURS] & 0b00011111);
			if((registers[ALARM2_HOURS] & BIT(5)) == 0){//am
				cout<<"am"<<endl;
			}//if
			else{//pm
				cout<<"pm"<<endl;
			}//else
		}//else
		
		cout<<"Alarm2 set minutes:"<<(registers[ALARM2_MINS] & 0b01111111)<<endl;
		
		if((registers[ALARM2_DAY_DATE] & BIT(6)) == 0){//Day/Date decision; Day = 1
			cout<<"Alarm2 set day:"<<(registers[ALARM2_DAY_DATE] & 0b00111111)<<endl;
		}//if
		else{//Day/Date decision; Date = 0
			cout<<"Alarm2 set date:"<<(registers[ALARM2_DAY_DATE] & 0b00111111)<<endl;
		}//else
		
}//readSetAlarms

/* Enable Alarm for physical output 
 * @param: alarmInterruptEnable: Which alarm to enable: Alarm 1/2/Both*/
void RTCDevice::setRtcInterrupt(unsigned int alarmInterruptEnable){
	this->registers = this->readRegisters(BUFFER_SIZE, 0x00);
	registers[CONTROL_REG] = (registers[CONTROL_REG] & 0b11111000); //Clear previous set alarms
	
	switch(alarmInterruptEnable){
		case ENABLE_ALARM1: {
			this->writeReg(CONTROL_REG, (registers[CONTROL_REG] | 0b00000001));
			cout<<"Alarm1 INT output enabled."<<endl;
		}
		break;
		case ENABLE_ALARM2: {
			this->writeReg(CONTROL_REG, (registers[CONTROL_REG] | 0b00000010));
			cout<<"Alarm2 INT output enabled."<<endl;
		}
		break;
		case ENABLE_BOTH_ALARM: {
			this->writeReg(CONTROL_REG, (registers[CONTROL_REG] | 0b00000011));
			cout<<"Alarm1&2 INT output enabled."<<endl;
		}
		case DISABLE_BOTH_ALARM: {
			this->writeReg(CONTROL_REG, (registers[CONTROL_REG] | 0b00000011));
			cout<<"Alarm1&2 INT output disabled."<<endl;
		}
		break;
		
		default:
		break;
	}//switch(alarmInterruptEnable)

	this->writeReg(CONTROL_REG, (registers[CONTROL_REG] | 0b00000100));//set INCTN

}//setRtcInterrupt

/* Output Square wave on BBSQW pin-out of RTC*
 * @param: Frequency of output wave on BBSQW pin-out of RTC Device
*/
void RTCDevice::squareWaveGenFunc(unsigned int freq){
	this->registers = this->readRegisters(BUFFER_SIZE, 0x00);
	
	registers[CONTROL_REG] = (registers[CONTROL_REG] & 0b10100011); //Clear previous set alarms
	registers[CONTROL_REG] = (registers[CONTROL_REG] | 0b01000000); //Enable BBSQW
	
	switch(freq){ 
		case SQW_KHZ_1:{
			this->writeReg(CONTROL_REG, (registers[CONTROL_REG] | 0b00000000));
			cout<<"1khz square-wave on SQW output enabled."<<endl;
		}
		break;
		case SQW_KHZ_1024:{
			this->writeReg(CONTROL_REG, (registers[CONTROL_REG] | 0b00001000));
			cout<<"1.024khz sqauare-wave on SQW output enabled."<<endl;
		}
		break;
		case SQW_KHZ_4096:{
			this->writeReg(CONTROL_REG, (registers[CONTROL_REG] | 0b00010000));
			cout<<"4.096khz square-wave on SQW output enabled."<<endl;
		}
		break;
		case SQW_KHZ_8192:{
			this->writeReg(CONTROL_REG, (registers[CONTROL_REG] | 0b00011000));
			cout<<"8.192khz square-wave on SQW output enabled."<<endl;
		}
		case SQW_STOP:{
			this->writeReg(CONTROL_REG, (registers[CONTROL_REG] & 0b10101111));
			cout<<"SQW output disabled."<<endl;
		}
		break;
		default: break;	
		}//switch(freq)

}//squareWaveGenFunc()

/**Displays the status of Oscillator Flag, Alarm 1 and Alarm 2 status
 * @return flag: 0 for no Alarm, 1 for Alarm1, 2 for Alarm 2
 */
void RTCDevice::getStatusRegister(){

	this->registers = this->readRegisters(BUFFER_SIZE, 0x00);
	
	if(registers[STATUS_REG] & BIT(7)){
		cout<<"OSF BIT:SET"<<endl;
	}//if
	else{
		cout<<"OSF BIT:RESET"<<endl;
	}//else
	
	if(registers[STATUS_REG] & BIT(3)){
		cout<<"EN32kHz BIT:SET"<<endl;
	}//if
	else{
		cout<<"EN32kHz BIT:RESET"<<endl;
	}//else
	
	if(registers[STATUS_REG] & BIT(1)){
		cout<<"Alarm2Flag:SET"<<endl;
	}//if
	else{
		cout<<"Alarm2Flag:RESET"<<endl;
	}//else
	
	if(registers[STATUS_REG] & BIT(0)){
		cout<<"Alarm1Flag:SET"<<endl;
	}//if
	else{
		cout<<"Alarm1Flag:RESET"<<endl;
	}//else
}//getStatusRegister()

/** Aging offset register takes a user-provided value to add or subtract from the codes
 * in the capacitance array registers
 * @param offset: aging offset register
 */
void RTCDevice::writeAgingOffset(signed char offset){
	this->writeReg(AGING_OFFSET_REG, offset);
}//writeAgingOffset()

/**Print all the registers of the device
 * @param: No of buffer
 */
void RTCDevice::dumpAll(unsigned int buf){
	this->debugDumpRegisters(buf);
}

/*Destructor*/
RTCDevice::~RTCDevice() {}

}//namespace rpi



