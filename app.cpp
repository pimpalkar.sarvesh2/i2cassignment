#include "header.h"
#include "i2cDriver.h"
#include "DS3231.h"

using namespace std;
using namespace rpi;


int main(){
	unsigned alarm;
	RTCDevice rtc(1,0x68);
	rtc.readTimeAndDate();
	rtc.readTemperature();
	
	//Arguments: setTime(bool twelveHoursFormat, bool am, hours, mins, secs)
	rtc.setTime(1,0,11,59,00);
	 	
	//Arguments: setDate(dateOfMonth,month,year,dayOfWeek)
	rtc.setDate(3,3,20,7); 	
	
	//Arguments: setAlarm(twelveHoursFormat,alarmNo,hours, mins, secs, dayOfWeekORdateOfMonth, alarmType)
	rtc.setAlarm(0, 1, 0, 0, 0, 0, ALARM2_WHEN_HM_MATCH);
	rtc.setAlarm(1, 0, 0, 0, 0, 0, ALARM1_WHEN_DAY_HMS_MATCH);
	
	rtc.readSetAlarms();
	
	rtc.setRtcInterrupt(DISABLE_BOTH_ALARM);
	rtc.squareWaveGenFunc(SQW_KHZ_1);
	
	rtc.getStatusRegister();
	//Arguments: writeAgingOffset(offsetValue);
	//writeAgingOffset(1) //ToBeUsed when required
	
	rtc.dumpAll(19);
 return 0;
}
