#include "header.h"

//#define TestCode
#ifndef I2C_H_
#define I2C_H_

#define I2C_0 "/dev/i2c-0"
#define I2C_1 "/dev/i2c-1"



namespace rpi {

/**
 * @class I2CDevice
 * @brief Generic I2C Device class that can be used to connect to any type of I2C device and read or write to its registers
 */
class I2CDeviceDriver{
private:
	unsigned int bus;
	unsigned int device;
	int file;
public:
	I2CDeviceDriver(unsigned int bus, unsigned int device);
	virtual int openBus();
	virtual int write(unsigned char value);
	virtual unsigned char readReg(unsigned int registerAddress);
	virtual unsigned char* readRegisters(unsigned int number, unsigned int fromAddress=0);
	virtual int writeReg(unsigned int registerAddress, unsigned char value);
	virtual void debugDumpRegisters(unsigned int number = 0xff);
	virtual void closeBus();
	virtual ~I2CDeviceDriver();
};

} //rpi

#endif /* I2C_H_ */
